
// This is the Ajax Request for deleting Projects
$(document).ready(function(){
    $('#delete-product-btn').on('click', function() { 
        if(confirm('Are you sure you want to delete this')){
            let sku = [];
            $(':checkbox:checked').each(function(i){
                sku[i] = $(this).val();
            })
            if(sku.length === 0){
                alert('Please select atleast one checkbox to delete');
            }else{
                $.ajax({
                    url: 'deleteProduct.php',
                    method: 'POST',
                    data: {sku:sku},
                    success:function(){
                        for(let i = 0; i<sku.length; i++){

                            $('#' + sku[i]).remove()
                        }
                    }
                })
            }
        }else{
            return false;
        }
    });
})