$(document).ready(function(){
    // Here is the special attribute form manipulation

    let productType = $('#productType');
    let specialAttr = $('#specialAttribute');

    let disc = $(`<div class="col-auto">
    <label for="size" class="col-form-label">Size (MB)</label>
    <p class="text-danger size-validation validation"></p>
    <input type="number" name="size" class="form-control" id="size">
    </div>
    <h6 class="my-2">Please provide Disc size in MB</h6>`);

    let furniture = $(`<div class="col-auto">
        <label for="height" class="col-form-label">Height</label>
        <p class="text-danger height-validation validation"></p>
        <input type="number" step="0.1" name="height" class="form-control" id="height">
    </div>
    <div class="col-auto">
        <label for="width" class="col-form-label">Width</label>
        <p class="text-danger width-validation validation"></p>
        <input type="number" step="0.1" name="width" class="form-control" id="width">
    </div>
    <div class="col-auto">
        <label for="length" class="col-form-label">Length</label>
        <p class="text-danger length-validation validation"></p>
        <input type="number" step="0.1" name="length" class="form-control" id="length">
    </div>
    <h6 class="my-2">Please provide Dimensions (Height, Width and Length in M)</h6>`);

    let book = $(`<div class="col-auto">
        <label for="weight" class="col-form-label">Weight (KG)</label>
        <p class="text-danger weight-validation validation"></p>
        <input type="number" step="0.1" name="weight" class="form-control" id="weight">
    </div>
    <h6 class="my-2">Please provide Book weight in KG</h6>`);

    let attrArr = {'disc' : disc, 'furniture' : furniture, 'book' : book};
    
    productType.on('change', function(){
        
        specialAttr.empty();
        specialAttr.append(attrArr[productType.val()]);
    });
});
