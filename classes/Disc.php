<?php
namespace Product;


class Disc extends Product{
    // Here we add the special attributes for this class that are different from other Products
    public $attrUnit = 'MB';
    public $typeSlug = 'disc';

    public function __construct($arr){
        $attribute_value = $arr['size'];
        
        parent::__construct($arr['sku'], $arr['name'], $arr['price'], $attribute_value);
    }

}
?>
