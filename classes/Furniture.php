<?php
namespace Product;


class Furniture extends Product {
    // Here we add the special attributes for this class that are different from other Products
    public $attrUnit = 'HxWxL';
    public $typeSlug = 'furniture';

    public function __construct($arr){
        $attribute_value = $arr['height'] . 'x' . $arr['width'] . 'x' . $arr['length'];
        
        parent::__construct($arr['sku'], $arr['name'], $arr['price'], $attribute_value);
    }

}
?>
