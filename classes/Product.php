<?php
namespace Product;
use Product\Interfaces\ProductInterface;
use Product\Database\DB;
use \PDO;

// The main Product class
class Product implements ProductInterface{
    // Main properties for every product 
    public $sku;
    public $name;
    public $price;
    public $attribute_value;

    
    public function __construct($sku, $name, $price, $attribute_value){

        $this->sku = $sku;
        $this->name = $name;
        $this->price = $price;

        $this->attribute_value = $attribute_value;
    }

    // The database insert method
    public function insert(){
        DB::connect();
        $sql = "INSERT INTO products(type_id, sku, name, price, attribute_id, attribute_value) 
                VALUES(:type_id, :sku, :name, :price, :attribute_id, :attribute_value)";

        $type_id = self::getTypeId($this->typeSlug);
        $attribute_id = self::getAttributeId($this->attrUnit);
        $stmt = DB::$pdo->prepare($sql);
        $stmt->bindParam(":type_id", $type_id);
        $stmt->bindParam(":sku", $this->sku);
        $stmt->bindParam(":name", $this->name);
        $stmt->bindParam(":price", $this->price);
        $stmt->bindParam(":attribute_id", $attribute_id);
        $stmt->bindParam(":attribute_value", $this->attribute_value);

        $stmt->execute();
    }

    // Getting the type id compared to the slug provided, this method is used in the insert method
    public function getTypeId($typeSlug){
        DB::connect();
        $sql = "SELECT id FROM types WHERE type_slug = '$typeSlug' LIMIT 1";
        $stmt = DB::$pdo->query($sql);

        while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            return $row['id'];
        }

        
    }

    // Getting the attribute id compared to the attribute unit provided, this method is used in the insert method
    public function getAttributeId($attributeUnit){
        DB::connect();
        $sql = "SELECT id FROM attributes WHERE attribute_unit = '$attributeUnit' LIMIT 1";
        $stmt = DB::$pdo->query($sql);
        while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            return $row['id'];
        };
    }

    // This method is for the sku code validation
    public static function getProducts(){
        DB::connect();
        $data = [];
        $sql = "SELECT * FROM products INNER JOIN attributes ON products.attribute_id = attributes.id;";
        $stmt = DB::$pdo->query($sql);
        while($row = $stmt->fetch(PDO::FETCH_ASSOC)){
            $data[] = $row;
        };
        return $data;
    }

    // This function is for deleting the Products
    public static function deleteProduct($skuArr){
        DB::connect();
        $skuArr = "'" . implode ( $skuArr , "' OR sku = '" ) . "'";
        $sql = "DELETE FROM products WHERE sku = $skuArr";
        $stmt = DB::$pdo->query($sql);
        
    }
}








?>