<?php
namespace Product;



class Book extends Product {
    // Here we add the special attributes for this class that are different from other Products
    public $attrUnit = 'kg';
    public $typeSlug = 'book';

    public function __construct($arr){
        $attribute_value = $arr['weight'];
        
        parent::__construct($arr['sku'], $arr['name'], $arr['price'], $attribute_value);
    }

}
?>
