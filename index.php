<?php
require_once 'autoloader.php';


use Product\Product;

$products = Product::getProducts();

?>

<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">

    <!-- Personal CSS -->

    <link rel="stylesheet" href="assets/CSS/style.css">

    <title>Junior Dev Test</title>
</head>

<body>
    <div class="container">
        <div class="row mt-5">
            <div class="col-12 d-flex border-bottom pb-3">
                <h2>Product List</h2>
                <div class="d-flex ms-auto">
                    <a href="add_product.php" class="btn btn-primary me-3">ADD</a>
                    <button class="btn btn-danger" id="delete-product-btn">MASS DELETE</button>
                </div>
            </div>
        </div>
        <div class="row mt-4">
            <div class="col-12">
                <div class='row'>
                    <?php
                foreach($products as $product){ 
                    echo "<div class='col-4 mt-4' id='" . $product['sku'] . "'>
                    <div class='card' style='width: 22rem;'>
                    <div>
                    <input type='checkbox' name='prductSku[]' value='" . $product['sku'] . "' class='delete-checkbox ms-3 mt-4' data-sku-val='" . $product['sku'] . "'>
                    </div>
                    <div class='card-body text-center pb-5'>
                        <div class='sku'>
                            <h5>" . $product['sku'] . "</h5>
                        </div>
                        <div class='name'>
                            <h5>" . $product['name'] . "</h5>
                        </div>
                        <div class='price'>
                            <h5>" . round($product['price'], 2) . " $</h5>
                        </div>
                        <div class='specific-value'>
                            <h5>" . $product['attribute_name'] . ": " . $product['attribute_value'] . " (" . $product['attribute_unit'] . ")</h5>
                        </div>
                    </div>
                </div>
                </div>";
                }
                ?>
                </div>
            </div>
        </div>
    </div>

    <!-- Footer -->
    <footer class="text-center border-top border-bottom mt-4">
        <h4 class='py-3'>Scandiweb Test assignment</h4>
    </footer>

    <!-- Option 1: Bootstrap Bundle with Popper -->
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-gtEjrD/SeCtmISkJkNUaaKMoLD0//ElJ19smozuHV6z3Iehds+3Ulb9Bn9Plx0x4" crossorigin="anonymous">
    </script>

    <!-- Jquery -->
    <script src="https://code.jquery.com/jquery-3.6.0.min.js"
        integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

    <!-- Custom JS -->
    <script src="assets/JS/delete.js"></script>

</body>

</html>