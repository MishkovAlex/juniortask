<?php
// die(1);
require_once '../autoloader.php';


use Product\Product;
use Product\Disc;
use Product\Book;
use Product\Furniture;


if($_SERVER["REQUEST_METHOD"] == "POST"){
    // Main ADD logic using Factory pattern
    
  
    $productType = $_POST['product_type'];


    $productType = ucfirst(strtolower($productType));
    $classname = "Product\\" . $productType;
    $object = new $classname($_POST);
    $object->insert();
    header("Location: " . HOMEPAGE);
}




?>