<?php

require_once 'autoloader.php';

use Product\Database\DB;
use Product\Product;

if ($_POST["sku"]) {
    Product::deleteProduct($_POST["sku"]);
}

?>